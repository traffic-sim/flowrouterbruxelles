import datetime
import os
import sys
from datetime import datetime
from optparse import OptionParser

import pandas as pd
import schedule
import time
import xmltodict
import numpy as np
import config
import random
from BXLMobiliteService import BXLMobiliteService
from pandas.io.excel._base import ExcelWriter
from openpyxl import load_workbook


class BXLScenario:
    current_time = datetime.now().strftime("%H:%M:%S")

    @classmethod
    def prepare_filepath(cls):
        if os.path.exists(config.dataset_fname):
            os.remove(config.dataset_fname)
        if not os.path.exists(config.scenario_filepath):
            os.mkdir(config.scenario_filepath)
        if not os.path.exists(config.simulation_output_folder):
            os.mkdir(config.simulation_output_folder)

    @classmethod
    def flows_to_dataframe(cls):
        with open(config.sumo_flows_fname) as fd:
            # xml -> dict
            doc_flows = xmltodict.parse(fd.read())
            flows = doc_flows['additional']['flow']
            df_flows = pd.DataFrame(flows)
            df_sorted = df_flows.sort_values('@begin')
            pass
        pass

    @classmethod
    def write_info_csv(cls, fname="dataset_info.csv"):
        """
        Write a file containing information about the columns of the dataset file
        """
        info = {
            'qPKW': "The number of passenger cars that drove over the detector within this time period (start_time>>end_time)",
            'vPKW': "The average speed of passenger cars that drove over the detector within this time period in km/h",
            'occupancy': "Percentage of time the detector is covered by a vehicule (between start_time and end_time).",
            'start_time': "Timestamp of start time",
            'end_time': "Timestamp of end time.",
            'Detector': "A string holding the id of the detector this line describes; should be one of the ids used in <DETECTOR_FILE>",
            'Time': "The time period begin that this entry describes (in minutes)",
        }
        df = pd.DataFrame.from_dict(info, orient='index')
        df.to_csv(fname, index=True, header=False, sep="=")

    @classmethod
    def execute_map_detectors(cls):
        net_fname = config.net_fname
        detectors_fname = config.sensors_position_fname
        out_sumo_detectors_fname = config.sumo_add_detectors_fname

        # NOTE: mapDetectors seems to take interval in seconds
        command = 'python $SUMO_HOME/tools/detector/mapDetectors.py -n %s -d %s -o %s --interval %d' % (
            net_fname, detectors_fname, out_sumo_detectors_fname, config.interval * 60)
        print(f'Executing: {command}')
        os.system(command)

    @classmethod
    def execute_flowrouter(cls):
        """
        Execute flowrouter program. It calculates a set of routes and traffic flows from given detectors and their
        measurements on a given network.

        :param net_fname: the SUMO network file (in XML)
        :param detectors_fname: the SUMO additional file containing the definition of the detectors (in XML)
        :param veh_count_dataset: the CSV file containing the vehicles count to be used for determining the traffic flow.
            This file is the output of bruxellesmobilite_dataset_to_sumo(...) function
        :param output_routes_fname:
        :param output_flows_fname:
        :param begin_time:
        :return:
        """
        net_fname = config.net_fname
        detectors_fname = config.sumo_add_detectors_fname
        veh_count_dataset = config.sumo_dataset_fname
        output_routes_fname = config.sumo_route_fname
        output_flows_fname = config.sumo_flows_fname
        begin_time = config.start_time
        command = 'python $SUMO_HOME/tools/detector/flowrouter.py -n %s -d %s -f %s -o %s -e %s -b %d -i %d --verbose --vclass passenger --lane-based' % (
            net_fname, detectors_fname, veh_count_dataset, output_routes_fname, output_flows_fname, begin_time,
            config.interval)

        print('Executing: %s' % (command))
        os.system(command)

    @classmethod
    def execute_flow_from_routes(cls):
        """
        From SUMO website: 'This script does the reverse of flowrouter.py and dfrouter in generating a traffic counts
        for detectors from a route or flow file. It's main purpse It can also be used to compare the input counts with
        the outputs of flowrouter.py and dfrouter.'

        -d FILE, --detector-file=FILE
                            read detectors from FILE (mandatory)
        -r FILE, --routes=FILE
                            read routes from FILE (mandatory)
        -e FILE, --emitters=FILE
                            read emitters from FILE (mandatory)
        -f FILE, --detector-flow-file=FILE
                            read detector flows to compare to from FILE
        """
        detector_file = config.sumo_add_detectors_fname
        routes_file = config.sumo_route_fname
        emitters_file = config.sumo_flows_fname
        detector_flow_file = config.sumo_dataset_fname
        output_fname = config.performance_out_fname

        if os.path.exists(output_fname):
            os.remove(output_fname)

        command = 'python $SUMO_HOME/tools/detector/flowFromRoutes.py -d %s -r %s -e %s -f %s -i %d --ps true>> %s' % (
            detector_file, routes_file, emitters_file, detector_flow_file, config.interval, output_fname)

        print('Executing: %s' % (command))
        os.system(command)

    @classmethod
    def collect_data_from_bxl_mobilite(cls):
        """
        Invoke Bruxelles Mobilité service and collect data each [interval] minutes, and append the collected data to a
        csv file
        :return:
        """

        # define when the service should terminate
        # time_change = datetime.timedelta(hours=24)
        # alternately...
        time_change = datetime.timedelta(minutes=config.simulation_duration)
        until_time = datetime.datetime.now() + time_change

        # finally, repeat for N hours/minutes/seconds the invocation of BXL mobilité service to get data from sensors
        schedule.every(config.interval).minutes.until(until_time).do(BXLMobiliteService.get_bxlmobilite_data)

        while True:
            schedule.run_pending()
            if not schedule.jobs:
                break
            time.sleep(1)

    @classmethod
    def execute_generate_parking_lots(cls, probability=0.3):
        command = 'python $SUMO_HOME/tools/generateParkingAreas.py -n %s -p %f -r -o parkings.out.xml' % (
            config.net_fname, probability)
        os.system(command)

    @classmethod
    def execute_sumo(cls, flow_fname=config.sumo_flows_fname):
        sumo_command = 'sumo' if not config.with_gui else 'sumo-gui'
        begin_time = config.start_time
        end_time = config.start_time + (config.simulation_duration * 60)

        if config.with_gui:
            config_ui = """<viewsettings>
                        <scheme name=\"larger\">
                            <vehicles vehicleQuality=\"2\" vehicle_exaggeration=\"1\" vehicle_constantSize=\"1\"/>
                            <persons person_exaggeration=\"1\" person_constantSize=\"1\" personName_show=\"1\"/>
                        </scheme>
                    </viewsettings>"""
            with open('ui_config.xml', "w") as fd:
                fd.write(config_ui)
            fd.close()
        command = '%s -n %s +a %s,%s,%s -b %d -e %d --ignore-route-errors --summary %s ' \
                  '--statistic-output %s --tripinfo-output %s --scale %f %s -d 100 ' \
                  '--edgedata-output %s --lanedata-output %s --vehroute-output %s --no-warnings true --mesosim true' % (
                      sumo_command,
                      config.net_fname,
                      config.sumo_route_fname,
                      flow_fname,
                      config.sumo_add_detectors_fname,
                      begin_time,
                      end_time,
                      config.summary_out_fname,
                      config.stats_out_fname,
                      config.tripinfo_fname,
                      config.sumo_simulation_scale,
                      "-g ui_config.xml" if config.with_gui else "",
                      config.edge_data_fname,
                      config.lane_data_fname,
                      config.vehroute_data_fname)
        print('Executing: %s' % (command))
        os.system(command)

    @classmethod
    def select_subset_sensors(cls, bxl_mobilite_dataset_fname=config.dataset_fname, perc=100):
        df = pd.read_csv(bxl_mobilite_dataset_fname, sep=";")
        all_devices_id = df['Detector'].unique()
        random.shuffle(all_devices_id)
        return all_devices_id[: int(len(all_devices_id) * (perc / 100))]

    @classmethod
    def main(cls):
        cls.parse_args()
        # cls.prepare_filepath()
        if not os.path.exists(config.simulation_output_folder):
            os.mkdir(config.simulation_output_folder)
        """
        if config.do_retrieve_data_from_bxl_mobilite == True:
            if config.one_shot_query:
                BXLMobiliteService.get_bxlmobilite_data()
            else:
                BXLMobiliteService.collect_data_from_bxl_mobilite()"""

        if config.perc_sensors == 100:
            list_sensors = None  # consider all the available sensors
        else:
            list_sensors = cls.select_subset_sensors(perc=config.perc_sensors)

        if config.do_read_sensors_position_from_file:
            df_sensors_loc = BXLMobiliteService.get_bxlmobilite_sensors_position_from_file(list_sensors=list_sensors)
        else:
            df_sensors_loc = BXLMobiliteService.get_bxlmobilite_sensors_position(list_sensors=list_sensors)

        cls.execute_map_detectors()
        BXLMobiliteService.bruxellesmobilite_dataset_to_sumo(list_sensors=list_sensors)

        if config.do_execute_flowrouter:
            cls.execute_flowrouter()
        cls.execute_flow_from_routes()

        if config.do_run_sumo:
            cls.execute_sumo()

        cls.traffic_volumes_comparison_to_excel(df_sensors_loc,
                                                config.sumo_dataset_fname,
                                                config.summary_out_fname,
                                                output_fname=config.traffic_volume_comparison_fname)
        # cls.write_traffic_volumes_comparison_to_csv(config.sumo_dataset_fname,
        #                                            "scenario/output/summary.out.xml")  # "scenario/detector.out.xml")

    @classmethod
    def traffic_volumes_comparison_to_excel(cls, df_sensors_loc, validation_csv_fname, summary_output,
                                            output_fname=config.traffic_volume_comparison_fname,
                                            output_position_fname=config.traffic_volume_comparison_sensors_loc_fname):
        """
        :param df_sensors_loc: the list of sensors together with their location (lat/lon)
        :param validation_csv_fname: the dataset from BXL mobilité (in the format that can be given in input to SUMO)
        :param summary_output: the SUMO output generated from the detectors
        :param output_fname: output FILE
        :param output_position_fname: output FILE containing the location of the sensors used in each simulation
        :return:
        """
        rr = range(config.start_time, config.simulation_duration * 60, config.interval * 60)
        df_val = pd.read_csv(validation_csv_fname, sep=";")
        with open(summary_output) as fd_simulation:
            # xml -> dict
            doc = xmltodict.parse(fd_simulation.read())
            # dict -> dataframe
            df_sim = pd.DataFrame(doc['summary']['step'])
            hourly_sim_data = df_sim.iloc[rr]
            time_stamps = hourly_sim_data['@time'].to_numpy(dtype=float) // 60
            df_comparison = pd.DataFrame()
            for time_instant in time_stamps:
                all_real_values = df_val.loc[df_val['Time'] == time_instant]
                mean_qpkw = all_real_values.loc[:, 'qPKW'].mean()
                mean_sim_quantity = hourly_sim_data.loc[time_instant * 60]['@running']
                df_comparison[time_instant] = (mean_qpkw, mean_sim_quantity)

            df_comparison = df_comparison.T
            df_comparison = df_comparison.rename({0: 'Real', 1: 'Simulated'}, axis='columns')
            df_comparison.index.name = 'Sim Time (s)'

            writer = pd.ExcelWriter(output_fname, mode="a" if os.path.exists(output_fname) else "w")
            df_comparison.to_excel(writer, sheet_name=config.output_folder_name)
            writer.save()

            writer_loc = pd.ExcelWriter(output_position_fname,
                                        mode="a" if os.path.exists(output_position_fname) else "w")
            df_sensors_loc.to_excel(writer_loc, sheet_name=f"{config.output_folder_name}_sensors")
            writer_loc.save()

    @classmethod
    def write_traffic_volumes_comparison_to_csv(cls, validation_csv_fname, summary_output,
                                                output_data_fname=config.traffic_volume_comparison_fname):
        """
        :param validation_csv_fname: the dataset from BXL mobilité (in the format that can be given in input to SUMO)
        :param summary_output: the SUMO output generated from the detectors
        :param output_data_fname: output data FILE, one sheet per simulation
        :return:
        """
        rr = range(config.start_time, config.simulation_duration * 60, config.interval * 60)
        df_val = pd.read_csv(validation_csv_fname, sep=";")
        with open(summary_output) as fd_simulation:
            # xml -> dict
            doc = xmltodict.parse(fd_simulation.read())
            # dict -> dataframe
            df_sim = pd.DataFrame(doc['summary']['step'])
            hourly_sim_data = df_sim.iloc[rr]
            time_stamps = hourly_sim_data['@time'].to_numpy(dtype=float) // 60
            df_comparison = pd.DataFrame()
            for time_instant in time_stamps:
                all_real_values = df_val.loc[df_val['Time'] == time_instant]
                mean_qpkw = all_real_values.loc[:, 'qPKW'].mean()
                mean_sim_quantity = hourly_sim_data.loc[time_instant * 60]['@running']
                df_comparison[time_instant] = (mean_qpkw, mean_sim_quantity)

            df_comparison = df_comparison.T
            df_comparison = df_comparison.rename({0: 'Real', 1: 'Simulated'}, axis='columns')
            df_comparison.index.name = 'Sim Time (s)'
            df_comparison.to_csv(output_data_fname, sep=";", decimal=',')

    @classmethod
    def evaluate_simulation_score(cls, validation_csv_fname, simulation_output):
        """
        This is out f to minimize!

        :param validation_csv_fname: the dataset from BXL mobilité (in the format that can be given in input to SUMO)
        :param simulation_output: the SUMO output generated from the detectors
        :return:
        """
        df_val = pd.read_csv(validation_csv_fname, sep=";")
        with open(simulation_output) as fd_simulation:
            # xml -> dict
            doc = xmltodict.parse(fd_simulation.read())
            if 'interval' not in doc['detector']:
                return -1

            # dict -> dataframe
            df_sim = pd.DataFrame(doc['detector']['interval'])

            device_names_sim = df_sim['@id'].unique()
            device_names_val = df_val['Detector'].unique()
            device_names = np.intersect1d(device_names_sim, device_names_val)

            df = pd.DataFrame()
            df_composed = pd.DataFrame()
            for device_name in device_names:
                # first, trim the dataframe and extract the part related to current device
                values_sim = df_sim.loc[df_sim['@id'] == device_name]
                values_sim = values_sim.set_axis(range(len(values_sim)))
                device_values = df_val.loc[df_val['Detector'] == device_name]
                device_values = device_values.set_axis(range(len(device_values)))

                real_count = device_values['qPKW']
                simulated_count = pd.to_numeric(values_sim['@entered'])

                min_len = np.min([len(real_count), len(simulated_count)])
                real_count = real_count.iloc[:min_len]
                simulated_count = simulated_count.iloc[:min_len]
                df[device_name] = (real_count - simulated_count).abs()
                df_composed[device_name] = (real_count.to_list()[0], simulated_count.to_list()[0])
            out = df.mean(axis=0)  # .mean()
            return out, df_composed

    @classmethod
    def update_paths(cls):
        config.output_folder_name = f"output_{config.simulation_id}"
        config.simulation_output_folder = f'{config.scenario_filepath}/{config.output_folder_name}/'
        config.edge_data_fname = f'{config.simulation_output_folder}/edgedata.out.xml'
        config.lane_data_fname = f'{config.simulation_output_folder}/lanedata.out.xml'
        config.vehroute_data_fname = f'{config.simulation_output_folder}/vehroute.out.xml'
        config.stats_out_fname = f'{config.simulation_output_folder}/stats.out.xml'
        config.tripinfo_fname = f'{config.simulation_output_folder}/tripinfo.out.xml'
        config.summary_out_fname = f'{config.simulation_output_folder}/summary.out.xml'
        config.performance_out_fname = f'{config.simulation_output_folder}/geh.out.csv'

    @classmethod
    def parse_args(cls):
        optParser = OptionParser()
        optParser.add_option("-n", "--net-file", dest="netfile", help="read SUMO network from FILE (MANDATORY)")
        optParser.add_option("-S", "--scenario_filepath", dest="scenario_filepath",
                             help="the FOLDER where the files necessary to run the simulation are saved")
        optParser.add_option("-O", "--simulation_output_folder", dest="simulation_output_folder",
                             help="the FOLDER where the files generated from the simulation are saved")
        optParser.add_option("-d", "--dataset_fname", dest="dataset_fname",
                             help="the FILE name of the dataset from Bruxelles mobilité")
        optParser.add_option("-s", "--sumo_dataset_fname", dest="sumo_dataset_fname",
                             help="the FILE name of the dataset from Bruxelles mobilité. THIS can be given in input to flowRouter.")
        optParser.add_option("-p", "--sensors_position_fname", dest="sensors_position_fname",
                             help="the FILE containing the coordinates of the sensors from Bruxelles mobilité")
        optParser.add_option("-x", "--sumo_add_detectors_fname", dest="sumo_add_detectors_fname",
                             help="the XML FILE generated by mapDetectors tool. This contains the sensors definition and can be provided as additional file to SUMO")
        optParser.add_option("-r", "--sumo_route_fname", dest="sumo_route_fname", help="the FILE containing the routes")
        optParser.add_option("-f", "--sumo_flows_fname", dest="sumo_flows_fname", help="the FILE containing the flows")
        optParser.add_option("-l", "--perf_fname", dest="sumo_perf_fname",
                             help="the FILE containing the GEH statistics measure (calculated using flowFromRoutes.py tool)")

        optParser.add_option("-g", "--with-gui", dest="with_gui", default=False,
                             help="if -z is true and -g is true, then SUMO is executed with the graphical UI (default: false)")
        optParser.add_option("-t", "--do-routing", dest="do_routing", default=True,
                             help="if true, execute flowRouter.py script for computing traffic (default: true)")
        optParser.add_option("-z", "--do-simulation", dest="do_sim", help="if true, execute SUMO (default: true)",
                             default=True)
        optParser.add_option("-w", "--do-get-data", dest="do_get_data", default=True,
                             help="if true, get the real traffic data from public BXL Mobilité API (https://data.mobility.brussels/fr/info/traffic_live_geom/, default: true)")

        optParser.add_option("-P", "--perc_sensors", dest="perc_sensors", default=100,
                             help="Percentage of sensors (from BXL Mobilité) to use for building the traffic model (Default: 100)")

        optParser.add_option("-F", "--read-sensors-fname", dest="read_sensors_fname", default=False,
                             help="Read the location of the sensors from file (Default: False). If false, the BXL mobilité service will be invoked via the REST API")

        optParser.add_option("-I", "--simulation-id", dest="simulation_id", default='0',
                             help="An integer that identify this simulation (Default: 0)")

        (options, args) = optParser.parse_args()

        if not options.netfile:
            optParser.print_help()
            sys.exit()

        if options.scenario_filepath:
            config.scenario_filepath = options.scenario_filepath
        if options.simulation_output_folder:
            config.simulation_output_folder = options.simulation_output_folder
        if options.dataset_fname:
            config.dataset_fname = options.dataset_fname
        if options.sumo_dataset_fname:
            config.sumo_dataset_fname = options.sumo_dataset_fname
        if options.sensors_position_fname:
            config.sensors_position_fname = options.sensors_position_fname
        if options.sumo_add_detectors_fname:
            config.sumo_add_detectors_fname = options.sumo_add_detectors_fname
        if options.sumo_route_fname:
            config.sumo_route_fname = options.sumo_route_fname
        if options.sumo_flows_fname:
            config.sumo_flows_fname = options.sumo_flows_fname
        if options.sumo_perf_fname:
            config.performance_out_fname = options.sumo_perf_fname

        if options.read_sensors_fname:
            config.do_read_sensors_position_from_file = True

        """
        if options.perc_sensors:
            config.perc_sensors = eval(options.perc_sensors)
        if options.simulation_id:
            config.simulation_id = eval(options.simulation_id)"""
        cls.update_paths()

        if options.with_gui:
            config.with_gui = options.with_gui
        if options.do_routing:
            config.do_execute_flowrouter = options.do_routing
        if options.do_sim:
            config.do_run_sumo = options.do_sim
        if options.do_get_data:
            config.do_retrieve_data_from_bxl_mobilite = options.do_get_data


if __name__ == '__main__':
    BXLScenario.main()
