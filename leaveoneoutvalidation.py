import os

if __name__ == '__main__':
    perc_sensors = 25

    for id_exp in range(2):
        command = f"python main.py -n scenario/bxl_full.net.xml -P {perc_sensors} -F True -I {id_exp}"
        print("------------------------------------")
        print(f"Executing: {command}")
        print("------------------------------------")
        os.system(command)
