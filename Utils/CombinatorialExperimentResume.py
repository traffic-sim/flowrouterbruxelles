import numpy as np
import pandas as pd
from sklearn.metrics import mean_absolute_error


def get_cumulative_mean(fname="data/traffic_volume_comparison.xlsx"):
    xl = pd.ExcelFile(fname)
    maes = []
    cum_mean_values = []
    for sheet in xl.sheet_names:
        df = pd.read_excel(fname, sheet_name=sheet)
        df = df.set_index('Sim Time (s)')
        latest_mean_value = mean_absolute_error(df['Real'].to_numpy(), df['Simulated'].to_numpy())
        maes.append(latest_mean_value)
        cum_mean_values.append(np.mean(maes))
    return cum_mean_values


def get_mae(fname="data/traffic_volume_comparison.xlsx"):
    xl = pd.ExcelFile(fname)
    maes = []
    for sheet in xl.sheet_names:
        df = pd.read_excel(fname, sheet_name=sheet)
        df = df.set_index('Sim Time (s)')
        maes.append(mean_absolute_error(df['Real'].to_numpy(), df['Simulated'].to_numpy()))
    return np.mean(maes)


if __name__ == '__main__':
    mae_25 = get_mae("data/out_25perc/traffic_volume_comparison.xlsx")
    mae_50 = get_mae("data/out_50perc/traffic_volume_comparison.xlsx")
    mae_75 = get_mae("data/out_75perc/traffic_volume_comparison.xlsx")
    mae_100 = get_mae("data/out_100perc/traffic_volume_comparison.xlsx")

    cum_mean_25 = get_cumulative_mean("data/out_25perc/traffic_volume_comparison.xlsx")
    cum_mean_50 = get_cumulative_mean("data/out_50perc/traffic_volume_comparison.xlsx")
    cum_mean_75 = get_cumulative_mean("data/out_75perc/traffic_volume_comparison.xlsx")
    cum_mean_100 = get_cumulative_mean("data/out_100perc/traffic_volume_comparison.xlsx")
    print('ok')
